"""This module encapsulates the more complicated workflow steps.

The main purpose is to keep the CLI module a bit cleaner.
"""
from typing import Optional

from hifis_wolito.components.certificate_generator import CertificateGenerator
from hifis_wolito.components.email_handling import Email
from hifis_wolito.components.signature import Signature
from hifis_wolito.model.participant import Participant
from hifis_wolito.model.workshop import Workshop
from hifis_wolito.settings import Settings


def certificate_generation(
        current_workshop: Workshop, generate_all: bool,
        enable_contribution_descriptions: bool
) -> None:
    """Run the certificate generation process for a given workshop.

    Args:
        current_workshop:
            The workshop for which to generate the certificates.
        generate_all:
            On `True` all certificates will be re-created,
            on `False` only missing certificates are generated.
        enable_contribution_descriptions:
            Whether to include contribution descriptions.
    """
    # === Figure out for whom to generate certificates ===
    if generate_all:
        requires_certificate = [
            participant for participant in current_workshop.participants
            if participant.checked_in
        ]
    else:
        requires_certificate = [
            participant for participant in current_workshop.participants
            if participant.checked_in and not participant.has_certificate
        ]
    # === Generate the certificates ===
    pdf_signature = Signature.from_yaml(current_workshop.signature_config)

    generator = CertificateGenerator(
        signature=pdf_signature,
        tex_template_file=current_workshop.certificate_template,
        contribution_descriptions=enable_contribution_descriptions
    )

    generator.generate_certificates(
        workshop=current_workshop,
        participants=requires_certificate
    )

    # Save the updated participant information
    current_workshop.store_to_disk()


def prepare_certificate_emails(
        current_workshop: Workshop,
        send_to_all: bool,
        cc_addresses: Optional[list[str]] = None
) -> dict[Participant, Email]:
    """Prepare the certification emails to be sent out to the participants.

    Args:
        current_workshop:
            The workshop that is being processed.
        send_to_all:
            Whether to also prepare emails for those that already have received
            one before.
        cc_addresses:
            (Optional) Recipients of a carbon copy for each sent certificate.
    Returns:
        A dictionary mapping the participants to the respective email they
        should receive.
    """
    settings = Settings.get_current_settings()

    # === Figure out to whom to send emails ===
    if send_to_all:
        email_recipients = [
            participant for participant in current_workshop.participants
            if participant.has_certificate
        ]
    else:
        email_recipients = [
            participant for participant in current_workshop.participants
            if participant.has_certificate and not participant.certificate_sent
        ]

    # === Prepare the emails ===
    with open(current_workshop.email_template) as input_stream:
        email_template = input_stream.read()
    email_template = email_template.replace(
        "<!--event_title!-->", current_workshop.title
    )
    email_template = email_template.replace(
        "<!--reply_address!-->", settings.email.reply_to
    )

    footer_fragment = current_workshop.email_footer
    if footer_fragment.exists():
        with open(footer_fragment) as input_stream:
            footer = input_stream.read()
    else:
        footer = ""

    email_template.replace("<!--footer!-->", footer)

    prepared_emails = {}
    for recipient in email_recipients:
        prepared_emails[recipient] = Email(
            sender=settings.email.sender,
            recipient=recipient.email,
            reply_to=settings.email.reply_to,
            subject=f"Your workshop certificate for {current_workshop.title}",
            content=email_template.replace("<!--name!-->", recipient.name),
            attachment_path=recipient.certificate,
            cc_to=cc_addresses
        )
    return prepared_emails
