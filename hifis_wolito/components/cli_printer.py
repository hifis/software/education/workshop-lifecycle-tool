from datetime import timedelta

import click
from prettytable import PrettyTable, prettytable
from hifis_wolito.model.participant import Participant
from hifis_wolito.model.workshop import Workshop


class CliPrinter:

    class Symbol:
        YES = "o"
        NO = ""
        NONE = "---"

    @staticmethod
    def print_participant_states(participants: list[Participant]) -> None:
        """ Print a table overview of the participant states of a workshop."""

        symbol = CliPrinter.Symbol

        overview = PrettyTable(
            field_names=[
                "Name", "Email", "Registration tags", "Checked in",
                "Certificate", "E-Mail sent"
            ]
        )
        overview.set_style(prettytable.SINGLE_BORDER)

        for column in ["Name", "Email", "Registration tags", "Certificate"]:
            overview.align[column] = "l"

        participants.sort(key=lambda p: p.name)
        for participant in participants:
            overview.add_row([
                participant.name, participant.email, participant.pretty_tags,
                symbol.YES if participant.checked_in else symbol.NO,
                participant.certificate.name
                if participant.certificate is not None else symbol.NONE,
                symbol.YES if participant.certificate_sent else symbol.NO
            ])
        click.echo(overview)

    @staticmethod
    def print_workshop_overview(workshops: list[Workshop]) -> None:

        overview = PrettyTable(
            field_names=["ID", "Date", "Title", "Duration"]
        )
        overview.set_style(prettytable.SINGLE_BORDER)

        workshops.sort(key=lambda w: w.start)
        for workshop in workshops:
            overview.add_row([
                workshop.indico_id,
                workshop.pretty_date(),
                workshop.title,
                workshop.pretty_duration()
            ])
        click.echo(overview)

    @staticmethod
    def print_workshop_details(workshop: Workshop) -> None:
        click.echo(click.style(workshop.title, bold=True))
        for session in workshop.sessions:
            click.echo(f"{session} ({session.pretty_duration()})")
            for contribution in session.contributions:
                click.echo(
                    f"* {contribution.title} ({contribution.pretty_duration()})"
                )

        participant_count = len(workshop.participants)
        no_shows = len([p for p in workshop.participants if not p.checked_in])

        total_participant_hours = timedelta()
        session_times = PrettyTable(
            field_names=[
                "Session", "Duration", "Participants", "Participant Hours"
            ]
        )
        for session in workshop.sessions:
            visitors = len([
                p for p in workshop.participants
                if (session in workshop.certifiable_sessions(p))
            ])
            session_participant_hours = session.duration * visitors
            session_times.add_row([
                session.title, session.duration,
                visitors, session_participant_hours
            ])
            total_participant_hours += session_participant_hours

        click.echo(session_times)
        click.echo(f"Workshop hours (wall): {workshop.pretty_duration()}")
        click.echo(f"Total Participant-hours: {total_participant_hours}")
        click.echo(f"{participant_count} registrations, ({no_shows} no show)")
