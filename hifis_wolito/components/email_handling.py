import smtplib
import ssl

from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from getpass import getpass
from pathlib import Path
from typing import Optional


class Email:
    """A convenience wrapper for plain-text emails with optional attachment.

    Only a single recipient per message is supported.
    """

    TOKEN_FROM = "From"
    TOKEN_TO = "To"
    TOKEN_SUBJECT = "Subject"
    TOKEN_REPLY_TO = "Reply-To"
    TOKEN_CC_TO = "Cc"

    def __init__(
            self,
            sender: str,
            recipient: str,
            subject: str,
            content: str,
            reply_to: Optional[str] = None,
            cc_to: Optional[list[str]] = None,
            attachment_path: Optional[Path] = None
    ):
        if not sender:
            raise ValueError("Missing sender for email")
        if not recipient:
            raise ValueError("Missing recipient for email")
        if not subject:
            raise ValueError("Missing subject for email")

        self._message = MIMEMultipart()
        self._message[Email.TOKEN_FROM] = sender
        self._message[Email.TOKEN_TO] = recipient
        self._message[Email.TOKEN_SUBJECT] = subject
        self._message.attach(MIMEText(content, "html"))

        if cc_to:
            self._message.add_header(Email.TOKEN_CC_TO, ",".join(cc_to))

        if reply_to:
            self._message.add_header(Email.TOKEN_REPLY_TO, reply_to)

        # Deal with Attachment
        if (
            attachment_path is not None
            and attachment_path.is_file()
            and attachment_path.exists()
        ):
            with open(attachment_path, "rb") as attachment:
                part = MIMEBase("application", "octet-stream")
                part.set_payload(attachment.read())
            encoders.encode_base64(part)
            part.add_header(
                "Content-Disposition",
                f"attachment; filename={attachment_path.name}",
            )
            part.add_header(
                "Content-Type",
                f"application/pdf; name={attachment_path.name}"
            )
            self._message.attach(part)

    @property
    def message(self) -> MIMEMultipart:
        """Get the underlying message object.

        Returns:
            the underlying raw object
        """
        return self._message

    def __str__(self) -> str:

        summary = "=== Start of Message ===\n"
        for key in self._message.keys():
            summary += f"{key}: {self._message[key]}\n"
        summary += "=== End of Message ===\n\n"
        return summary


class MailServer:
    """A convenience wrapper around handling a simple SMTP mail server."""

    def __init__(self, host: str, port: int = 465):
        self._is_logged_in = False
        self.host = host
        self.port = port
        self._connection = smtplib.SMTP_SSL(
            host=host,
            port=port,
            context=ssl.create_default_context()
        )

    def __del__(self):
        self._connection.close()

    def login(self):
        """Log in to the mail server.
        Will query for username and password on the command line while doing so.
        """
        print(f"Logging in to Mailserver {self.host}:{self.port}")
        username = input("Username: ")
        password = getpass("Password: ")
        self._connection.login(user=username, password=password)
        self._is_logged_in = True

    def send_email(self, email: Email) -> None:
        """Send a given email.

        Requires to be logged in to the server first.

        Args:
            email:
                The email you want to send.

        Raises:
            RuntimeError:
                If not previously logged in to the mail server.
                Call MailServer.login() beforehand.
        """

        if not self._is_logged_in:
            raise RuntimeError(
                "Can not send e-mail - not logged in to mail server"
            )
        self._connection.send_message(email.message)
