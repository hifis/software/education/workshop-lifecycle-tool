import logging
import os
import subprocess
import tempfile
from datetime import timedelta
from pathlib import Path
from typing import Optional

import click

from hifis_wolito.settings import Settings
from hifis_wolito.constants import SUFFIX_PDF, SUFFIX_TEX
from hifis_wolito.components.signature import Signature
from hifis_wolito.model.participant import Participant
from hifis_wolito.model.session import Session
from hifis_wolito.model.workshop import Workshop
from hifis_wolito.util import escape_special_characters, pretty_duration


class XelatexToken:
    """The tokens used in the certificate template file."""
    PARTICIPANT_NAME = "%participant_name%"
    EVENT_TITLE = "%event_title%"
    EVENT_DATE = "%event_date%"
    EVENT_DURATION = "%event_duration%"
    EVENT_DESCRIPTION = "%event_description%"
    IMAGE_PATH = "%image_path%"
    ORGANIZER_LOGO = "%organizer_logo%"

    CONFIG_USE_SESSION_DURATION = "%config:use_session_duration%"


class CertificateGenerator:

    def __init__(
            self, signature: Signature, tex_template_file: Path,
            xelatex: Optional[Path] = None,
            contribution_descriptions: bool = True
    ):
        """Initialize the XeLaTeX driver that controls the generation of PDFs.

         Args:
             signature:
                The signature to be used to sign the PDFs.

            tex_template_file:
                The file containing the template for the certificates.

            xelatex:
                (Optional) Path to the XeLaTeX executable. If not specified,
                the path from the settings will be used instead.

            contribution_descriptions:
                (Optional) Whether to include contribution descriptions in the
                generated output. Enabled by default.
        """
        settings = Settings.get_current_settings()

        self.xelatex = (
            xelatex if xelatex
            else settings.general.xelatex_executable
        )
        self.tex_template_file = tex_template_file
        self.signature = signature
        self.contribution_descriptions = contribution_descriptions

        if not self.xelatex.exists():
            raise ValueError(
                f"Failed to find XeLaTeX executable at {self.xelatex}:"
                f"File does not exist"
            )

        if not self.tex_template_file.exists():
            raise ValueError(
                f"Failed to find certificate template at "
                f"{self.tex_template_file}:"
                f"File does not exist"
            )

    def generate_certificates(
            self, workshop: Workshop,
            participants: Optional[list[Participant]] = None
    ) -> None:
        """
        Generate the certificate file for given participants of a workshop

        The generated certificates will be stored in the participant objects,
        don't forget to save the workshop.

        Args:
            workshop:
                The workshop for which to generate certificates.
            participants:
                (Optional) A selected set of participants for which to generate
                the certificates. If not given, all checked-in participants of
                the workshop will be processed instead.
        """
        # If no participants are explicitly given, select all eligible ones
        if not participants:
            participants = [
                participant for participant in workshop.participants
                if participant.checked_in
            ]

        # Fetch the certificate template
        with open(self.tex_template_file) as output_stream:
            tex_template: str = output_stream.read()

        with tempfile.TemporaryDirectory() as working_dir:
            for participant in participants:
                try:
                    click.echo(
                        f"Generating certificate for {participant.name}: Tex",
                        nl=False
                    )
                    # Fill in the template with specific information
                    generated_tex = self.fill_in_tex_template(
                        template=tex_template,
                        workshop=workshop,
                        participant=participant,
                        output_directory=Path(working_dir)
                    )

                    click.echo(" → PDF", nl=False)
                    # Render the PDF
                    generated_pdf = self.render_pdf(
                        tex_file=generated_tex,
                        working_directory=Path(working_dir)
                    )

                    # TODO Find a way to detect if XeLaTeX got stuck,
                    # print an error, save the generated TeX and continue
                    # to next participant

                    click.echo(" → Sign", nl=False)
                    # Sign the generated certificate
                    signed_pdf = self.signature.sign_pdf(generated_pdf)
                    result_file = workshop.certificate_path(participant)
                    signed_pdf.replace(result_file)
                    participant.certificate = result_file

                    click.echo(" Done.")
                except ValueError:
                    click.echo(" → × Error.")

    def render_pdf(
            self, tex_file: Path,
            working_directory: Optional[Path] = None,
            passes: int = 2
    ) -> Path:
        """
        Run XeLaTeX to generate the pdf from a tex-file.

        Args:
            tex_file:
                The tex-file to process

            working_directory:
                (Optional) the working directory where the
                XeLaTeX helper files will end up. When batch processing, setting
                this to a temp-directory is a good idea. If not specified, the
                current working directory will be kept.

            passes:
                (Default: 2) How many passes XeLaTeX is supposed to make.
                Depending on the contents of the tex-file multiple passes may be
                required to get all the cross-references right.

        Returns:
            The path to the newly generated PDF
        """
        if not working_directory:
            working_directory = Path.cwd()

        with open(os.devnull, "w") as dev_null:  # Discard the messy stdout
            for _ in range(passes):
                subprocess.run(
                    args=[self.xelatex, tex_file],
                    stdout=dev_null,
                    cwd=working_directory
                )
        generated_pdf = working_directory / (tex_file.stem + SUFFIX_PDF)

        if not generated_pdf.exists():
            raise RuntimeError(f"Failed to generate {generated_pdf}")

        return generated_pdf

    def fill_in_tex_template(
            self,
            template: str,
            workshop: Workshop,
            participant: Participant,
            output_directory: Path
    ) -> Path:
        """
        Fills in the TeX template by replacing the placeholders in it.
        Args:
            template:
                The template text with the placeholders in it.
            workshop:
                The workshop for which to fill in the data.
            participant:
                the participant for which to fill in the data.
            output_directory:
                The directory into which to put the generated Tex-file.

        Returns:
            A path to the file containing the filled in template.
        Raises:
            ValueError: If the participant is not eligible for any sessions
            to be certified.
        """
        settings = Settings.get_current_settings()
        token = XelatexToken

        name = escape_special_characters(participant.name)
        title = escape_special_characters(workshop.title)
        date = workshop.pretty_date()

        eligible_sessions = workshop.certifiable_sessions(participant)
        if not eligible_sessions:
            error_message = f"{participant.name} not eligible for certification"
            logging.warning(error_message)
            raise ValueError(error_message)

        duration = (
            pretty_duration(
                sum([s.duration for s in eligible_sessions], start=timedelta())
            )
            if token.CONFIG_USE_SESSION_DURATION in template
            else workshop.pretty_duration()
        )

        description = self.typeset_description(eligible_sessions)

        xelatex_image_path = (
                "{" + str(workshop.image_folder.absolute()) + "/}"
        )
        # The image path in the certificate template is set via the
        # `\graphicspath` command from the `graphicx` package
        # There are some caveats regarding the formatting the correct input to
        # this command as documented in this post:
        # https://tex.stackexchange.com/a/139403

        replacements = {
            token.PARTICIPANT_NAME: name,
            token.EVENT_TITLE: title,
            token.EVENT_DATE: date,
            token.EVENT_DURATION: duration,
            token.EVENT_DESCRIPTION: description,
            token.IMAGE_PATH: xelatex_image_path,
            token.ORGANIZER_LOGO: workshop.organizer_logo.name
        }

        content: str = template
        for placeholder, replacement in replacements.items():
            content = content.replace(placeholder, str(replacement))

        # Write the content to a file
        generated_file = output_directory / (
                participant.file_name_slug + SUFFIX_TEX
        )

        with open(generated_file, "w") as input_stream:
            input_stream.write(content)

        logging.debug(f"Generated XeLaTex-template {generated_file}")
        return generated_file

    def typeset_description(self, sessions: list[Session]) -> str:
        sessions.sort(key=(lambda s: s.start))
        return "\n\n".join([
            session.as_tex(self.contribution_descriptions)
            for session in sessions if not session.is_default_session
        ])
