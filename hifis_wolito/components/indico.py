"""
This module provides Indico API Access to query for events.
"""
import logging
import requests
from pydantic_core import Url


class IndicoAPI:

    PROTOCOL = "https"
    HTTP_STATUS_OK = 200

    def __init__(self, endpoint_url: Url, token: str, api_key):
        """Set up the Indico API endpoint to be ready to serve queries.

        See https://docs.getindico.io/en/stable/http-api/access/

        Args:
            endpoint_url:
                The URL of the server providing the Indico system. If not
                specified otherwise, https will be assumed as the protocol.
            token:
                The OAuth API-token used to query the API.
            api_key:
                The old style URL-API key.
        """
        self.url = endpoint_url
        self.headers = {"Authorization": f"Bearer {token}"}
        self.api_key = api_key

    def query_event(self, event_id: int) -> dict:
        """Query the Indico API for event data.

        The event is queried at the session level.

        See Also:
            https://docs.getindico.io/en/stable/http-api/exporters/event/#sessions
        Args:
            event_id:
                The Indico-ID of the event

        Returns:
            A JSON-dictionary as delivered by the Indico API
        """

        rest_query = (
            f"{self.url}/export/event/{event_id}.json"
            f"?detail=sessions"
            f"&ak={self.api_key}"
        )

        response = requests.get(rest_query)  # (0)
        if response.status_code != self.HTTP_STATUS_OK:
            raise ValueError(f"Failed to obtain {rest_query}")

        logging.debug(f"Querying for event {event_id}")
        logging.debug(response.json())

        return response.json()
        # Note (0) In the export API call, no headers are used.
        # Sending the OAuth tokens is not required

    def query_registrations(self, event_id: int) -> dict:
        """Query the Indico API for registration data.

        See Also:
            https://docs.getindico.io/en/stable/http-api/access/#scopes
        Args:
            event_id:
                The Indico-ID of the event

        Returns:
            A JSON-dictionary containing the registrations for an event
            as delivered by the Indico API
        """

        rest_query = (
            f"{self.url}/api/events/{event_id}/registrants"
        )

        response = requests.get(rest_query, headers=self.headers)
        if response.status_code != self.HTTP_STATUS_OK:
            raise ValueError(f"Failed to obtain {rest_query}")

        logging.debug(f"Querying for registrations of {event_id}")
        logging.debug(response.json())

        return response.json()
