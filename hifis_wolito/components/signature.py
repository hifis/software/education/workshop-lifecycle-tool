# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: MIT

from datetime import datetime
from pathlib import Path

from cryptography.hazmat import backends
from cryptography.hazmat.primitives.serialization import pkcs12

from endesive.pdf import cms
from getpass import getpass

from yaml import safe_load


class Signature(object):
    """A container to bundle up all things required for signing a document."""
    def __init__(
            self, certificate_file: Path,
            signer_name: str, signer_email: str, signer_location: str,
            reason: str, page_coordinates, on_page=0, image=None, font_size=12
    ):
        """Populate the signature data according to provided information.

        Args:
            certificate_file:
                The file name for the certificate to be used for signing. The
                password for unlocking the file will be queried interactively
                from the command line.
            signer_name:
                The full name of the signer as to be displayed in the signature
                box.
            signer_email:
                The email to be used in the signing box.
            signer_location:
                The geographic location to be displayed in the signing box.
            reason:
                The reason for signing.
            on_page:
                (Default: 0 ) The page number of the PDF on which to put the
                signature. Counted from 0.
            page_coordinates:
                Location of the signature text box in PDF coordinates.
                Takes the form of the tuple (start_x, start_y, end_x, end_y),
                measured in points from the lower left page corner.
            image:
                (Optional) Path to the image file used for the signature.
            font_size:
                (Default: 12) Font size in point to use in the signature.
        Returns:
            A Signature object containing the signature data and the certificate
            which can be used for signing PDFs.
        """
        date = datetime.utcnow()
        human_readable_date = date.strftime("%Y-%m-%d, %H:%M UTC")
        signature_text = f"Signed by {signer_name}\n"\
                         f"{signer_location}, {human_readable_date}\n"\
                         f"{signer_email}"
        text_properties = {
            "fontsize": font_size
        }
        self.signature_data = {
            "aligned": 0,
            "sigflags": 3,
            "sigflagsft": 132,
            "sigbutton": False,
            "sigfield": "Signature1",
            "sigandcertify": True,
            "sigpage": on_page,
            "signaturebox": page_coordinates,
            "signature": signature_text,
            "contact": signer_email,
            "location": signer_location,
            "signingdate": date.strftime("D:%Y%m%d%H%M%S+00'00'"),
            "reason": reason,
            "password": None,
            "text": text_properties,
            }
        if image:
            self.signature_data["signature_img"] = image

        self.certificate_file = certificate_file
        self.certificate = self._load_certificate()

    def _load_certificate(self):
        """Loads the certificate file for signing the PDF.

        Will prompt for a password for the certificate on the command line.

        Returns:
            The unlocked certificate
        """
        with open(self.certificate_file, "rb") as cert_file:
            certificate_password = getpass(
                f"Password to unlock {self.certificate_file}: "
            )
            p12_certificate = pkcs12.load_key_and_certificates(
                data=cert_file.read(),
                password=certificate_password.encode(),
                backend=backends.default_backend()
            )

        certificate_entries = [
            distinguished_name.rfc4514_string()
            for distinguished_name in p12_certificate[1].subject.rdns
        ]
        print(f"Loaded certificate:{certificate_entries}")

        return p12_certificate

    @classmethod
    def from_yaml(cls, yaml_file: Path) -> "Signature":
        """Generate a signature from data given by a specific YAML file.

        Args:
            yaml_file:
                path to the YAML file containing the signature data
        Returns:
            A signature containing the data from the YAML file
        """
        with open(yaml_file) as input_stream:
            signature_data = safe_load(input_stream)
            # TODO Move the magic strings into their own constants
            name = signature_data["signer_name"]
            email = signature_data["signer_email"]
            location = signature_data["signer_location"]
            reason = signature_data["reason"]
            page_coordinates = (signature_data["page_coordinates"]["start_x"],
                                signature_data["page_coordinates"]["start_y"],
                                signature_data["page_coordinates"]["stop_x"],
                                signature_data["page_coordinates"]["stop_y"])
            certificate_file = signature_data["cert_file"]
            font_size = signature_data["font_size"]
        return Signature(
            certificate_file=certificate_file,
            signer_name=name,
            signer_email=email,
            signer_location=location,
            reason=reason,
            page_coordinates=page_coordinates,
            font_size=font_size
        )

    def sign_pdf(self, pdf_file: Path):
        """Sign a PDF file with a given signature and certificate.

        Will ask for the certificate's password in the console.
        Args:
            pdf_file: Path to the PDF file which is to be signed
        Return:
            The path of the signed pdf file
        """
        with open(pdf_file, "rb") as original_pdf:
            content = original_pdf.read()

        digital_signature = cms.sign(
            datau=content,
            udct=self.signature_data,
            key=self.certificate[0],
            cert=self.certificate[1],
            othercerts=self.certificate[2],
            algomd="sha256"
        )

        new_pdf_file = pdf_file.with_stem(pdf_file.stem + "-signed")
        with open(new_pdf_file, "wb") as signed_pdf:
            signed_pdf.write(content)
            signed_pdf.write(digital_signature)

        return new_pdf_file
