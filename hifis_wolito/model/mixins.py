from abc import ABC, abstractmethod
from datetime import timedelta, datetime
from typing import Optional

from babel.dates import format_date

from hifis_wolito.constants import DEFAULT_DATETIME
from hifis_wolito.util import pretty_duration


class TexRepresentable(ABC):

    @abstractmethod
    def as_tex(self, **kwargs) -> str:
        """
        Generate a TeX Representation of the object.

        Returns:
            A string containing the TeX code to represent the object.
        """
        pass


class LabelledTimespan (ABC):

    def __init__(
            self, title: Optional[str] = None,
            start: datetime = DEFAULT_DATETIME,
            end: datetime = DEFAULT_DATETIME,
            update_duration: bool = True
    ):
        self.title = title
        self._start = start
        self._end = end
        self._duration = timedelta()
        if update_duration:
            self._recalculate_duration()

    def _recalculate_duration(self):
        """A hook to be triggered to re-evaluate the duration.

        May be overridden for classes that define duration differently.
        When overriding, don't forget to set the `_duration`-attribute to the
        new value.
        """
        self._duration = self._end - self._start

    @property
    def duration(self) -> timedelta:
        return self._duration

    @property
    def start(self) -> datetime:
        return self._start

    @start.setter
    def start(self, new_value: datetime):
        self._start = new_value
        self._recalculate_duration()

    @property
    def end(self) -> datetime:
        return self._end

    @end.setter
    def end(self, new_value: datetime):
        self._end = new_value
        self._recalculate_duration()

    def pretty_duration(self) -> str:
        return pretty_duration(self.duration)

    def pretty_date(self) -> str:
        """Provide a period as a pretty-printed date or date range.

        For information on the babel format specifiers see:
        https://babel.pocoo.org/en/latest/dates.html#pattern-syntax

        Returns:
            The date or date range of the workshop printed in the form of
            ``01. January 2000`` or
            ``15. - 20. February 2000``.
            Details like month or year are added to the start date if it differs
            from the end date. Notice that all spaces are non-breaking.
        """
        end_part_format = "dd. MMMM YYYY"
        use_locale = "en"

        if self._start == self._end:  # Trivial case
            return format_date(self._start, end_part_format, locale=use_locale)

        different_month = self._start.month != self._end.month
        different_year = self._start.year != self._end.year

        start_part_format = "dd. "
        if different_month or different_year:
            start_part_format += "MMMM "
        if different_year:
            start_part_format += "YYYY "

        return (
            format_date(self._start, start_part_format, locale=use_locale)
            + "— " +
            format_date(self._end, end_part_format, locale=use_locale)
        )
