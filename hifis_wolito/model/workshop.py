import json
import logging
import shutil

import yaml

from datetime import datetime, timedelta
from pathlib import Path

from dateutil import parser

from hifis_wolito.components.indico import IndicoAPI
from hifis_wolito.constants import SUFFIX_PDF
from hifis_wolito.model.mixins import LabelledTimespan
from hifis_wolito.model.contribution import Contribution
from hifis_wolito.model.participant import Participant
from hifis_wolito.model.session import Session, SessionBlock
from hifis_wolito.settings import Settings


class Workshop(LabelledTimespan):

    # TODO should these go into the config as well?
    CERTIFICATES_SUBDIR = "certificates"
    DATA_FILE = "workshop.yml"

    class JsonToken:
        """Tokens used in the Indico APIs JSON."""
        CONTRIBUTIONS = "contributions"
        END = "endDate"
        RESULTS = "results"
        REGISTRANTS = "registrants"
        SESSIONS = "sessions"
        START = "startDate"
        TITLE = "title"

    class YamlToken:
        END = "end"
        PARTICIPANTS = "participants"
        SESSIONS = "sessions"
        START = "start"
        TITLE = "title"

    def __init__(
            self, indico_id: int,
            start: datetime, end: datetime,
            title: str = "",
            sessions: list[Session] = None,
            participants: list[Participant] = None
    ):
        """ Create a new workshop.

        Args:
            indico_id: The numeric ID used by Indico to identify the workshop.
            start: The start of the workshop according to Indico
            end: The end of the workshop according to Indico
            title: The workshops' title as given in Indico
            sessions: A list of session blocks that hold contributions.
            participants: A list of participants that joined the workshop.
        """
        super().__init__(
            title, start, end, update_duration=False
        )  # Do not update the duration yet, requires sessions to be set first

        # General Information
        self.indico_id = indico_id
        self.sessions: list[Session] = sessions if sessions else list()
        self._recalculate_duration()

        settings = Settings.get_current_settings()

        # Set the paths to related files and folders
        self.workshop_directory = settings.workshop_folder(self.indico_id)
        self.template_directory: Path = (
                self.workshop_directory /
                settings.templates.template_folder_name
        )
        self.certificate_directory: Path = (
                self.workshop_directory / self.CERTIFICATES_SUBDIR
        )
        self.data_file: Path = self.workshop_directory / self.DATA_FILE
        self.certificate_template = (
                self.template_directory /
                settings.templates.certificate_template
        )
        self.email_template: Path = (
                self.template_directory /
                settings.templates.email_text_template
        )
        self.email_footer: Path = (
            self.template_directory /
            settings.templates.email_footer_template
        )
        self.signature_config: Path = (
            self.template_directory /
            settings.general.signature_config_file
        )
        self.image_folder: Path = (
            self.template_directory if not settings.templates.image_folder_name
            else self.template_directory / settings.templates.image_folder_name
        )
        self.organizer_logo: Path = (
            self.image_folder / settings.templates.organizer_logo
        )

        # Add participants information
        self.participants = participants if participants else list()
        self._handle_duplicate_participants()

        # For each participant check the validity of certificate information
        # Depends on certificate paths being set up before correctly
        for participant in self.participants:
            expected = participant.certificate
            predicted = self.certificate_path(participant)
            found = predicted if predicted.exists() else None
            if expected and not found:  # Certificate file missing
                logging.warning(f"Missing certificate {expected}")
                participant.certificate = None
            elif not expected and found:  # Has certificate that is not logged
                logging.warning(f"Discovered unexpected certificate {found}")
                participant.certificate = found

    def _handle_duplicate_participants(self):

        entries_for_email = dict()
        for participant in self.participants:
            email = participant.email
            if email not in entries_for_email:
                entries_for_email[email] = participant
            else:  # Merge the participants
                entries_for_email[email].merge(participant)

        self.participants = list(entries_for_email.values())

    @property
    def contributions_by_session(self) -> dict[str, list[Contribution]]:
        return {
            session.title: session.contributions for session in self.sessions
        }

    @property
    def contributions_by_day(self) -> dict[datetime.date, list[Contribution]]:

        days = dict()

        for session in self.sessions:
            for contribution in session.contributions:
                day = contribution.start.date()
                if day not in days:
                    days[day] = list()
                days[day].append(contribution)
        return days

    @property
    def non_default_sessions(self) -> list[Session]:
        """Get all sessions in the workshop that are not default sessions."""
        return [
            session for session in self.sessions
            if not session.is_default_session
        ]

    def _recalculate_duration(self):
        """Get the duration of the workshop.

        This is the sum of the time from start to finish the workshop per day.

        Returns:
            The overall duration of the workshop as a timedelta.
        """

        daily_hours = []
        by_day = self.contributions_by_day
        for day, contributions in by_day.items():
            first = min(contributions, key=lambda c: c.start.time())
            last = max(contributions, key=lambda c: c.end.time())
            daily_hours.append(last.end - first.start)

        self._duration = sum(daily_hours, start=timedelta())  # See Note (0)
    # Note (0): The default start of the sum is `0`, which does not mix with
    # timedelta. Thus, we start explicitly with a timedelta as the first value.

    def create_structure(self):
        settings = Settings.get_current_settings()
        logging.info(f"Setting up new workshop in {self.workshop_directory}")

        # Create directory structure
        for directory in [
            self.workshop_directory, self.certificate_directory
        ]:
            directory.mkdir(exist_ok=True)

        # Initialize the template directory with the global templates
        shutil.copytree(
            src=settings.default_templates_folder,
            dst=self.template_directory
        )

    def certificate_path(self, participant: Participant) -> Path:

        file_name = participant.file_name_slug + SUFFIX_PDF
        return self.certificate_directory / file_name

    def certifiable_sessions(self, participant: Participant) -> list[Session]:
        """Find all sessions for which a participant is can be certified.

        The description will be generated from the contributions ordered in
        sessions.

        The session inclusion rules are as follows:

        - If a participant is not checked in they are not eligible for any
          certification
        - Default sessions are never included
        - If the participant has no tags, all sessions are included
        - If the participant has tags only sessions with a code that is
          matched by a tag are included

        The generated LaTeX-items are supposed to be put into a description
        environment in the template.

        Args:
            participant:
                The participant for whose certificate the description
                is intended.

        Returns:
            A list of sessions to include on the certificate
        """
        if not participant.checked_in:
            return []
        if not participant.tags:
            return self.sessions
        else:
            return [
                session for session in self.non_default_sessions
                if session.code in participant.tags
            ]

    @staticmethod
    def fetch_from_indico(
            indico_api: IndicoAPI, workshop_id: int, dump_json: bool = False
    ) -> "Workshop":
        """
        Fetch a workshops' information from Indico.

        Args:
            indico_api: The endpoint to query
            workshop_id: The numeric ID of the workshop as given by Indico
            dump_json: Also generate a dump of the processed JSON
        Returns:
            A workshop object according to the JSON data given by Indico.
        """
        settings = Settings.get_current_settings()
        token = Workshop.JsonToken
        event_json: dict = indico_api.query_event(workshop_id)
        participant_json: dict = indico_api.query_registrations(workshop_id)

        workshop_data = event_json[token.RESULTS][0]

        if dump_json:
            dump_folder = settings.workshop_folder(workshop_id)
            with open(dump_folder/"event.json", "w") as output_stream:
                json.dump(event_json,output_stream, indent=4)
            with open(dump_folder/"participant.json", "w") as output_stream:
                json.dump(participant_json,output_stream, indent=4)

        unsorted_contributions = [
            Contribution.from_indico_json(contribution_json)
            for contribution_json in workshop_data[token.CONTRIBUTIONS]
        ]

        default_session = Session(session_blocks=[
                SessionBlock(contributions=[contribution])
                for contribution in unsorted_contributions
        ])
        """ Unnamed sessions for storing non-teaching contributions
        Each unsorted contribution gets their own default session 
        since contributions can happen on different days but session blocks 
        only span one day.
        """

        sessions = [
                Session.from_indico_json(session_data)
                for session_data in workshop_data[token.SESSIONS]
        ]

        sessions.append(default_session)
        sessions = Session.compact(sessions)

        return Workshop(
            indico_id=workshop_id,
            start=parser.parse(str(workshop_data[token.START]), fuzzy=True),
            end=parser.parse(str(workshop_data[token.END]), fuzzy=True),
            title=workshop_data[token.TITLE],
            sessions=sessions,
            participants=[
                Participant.from_indico_json(entry)
                for entry in participant_json[token.REGISTRANTS]
            ]
        )

    @staticmethod
    def fetch_from_disk(workshop_id) -> "Workshop":
        """
        Load stored workshop information from the file system.

        Args:
            workshop_id: The numeric ID of the workshop as used by Indico.
        Return:
            A workshop instance containing the data from the corresponding
            YAML-file on the file system.
        """

        to_load: Path = (
                Settings.get_current_settings().workshop_folder(workshop_id) /
                Workshop.DATA_FILE
        )

        if not to_load.exists():
            raise ValueError(
                f"No data file for workshop {workshop_id}, nothing to load."
            )

        with open(to_load, mode="r") as input_stream:
            workshop_yaml = yaml.safe_load(input_stream)

        # TODO: load workshop state?

        return Workshop(
            indico_id=workshop_id,
            start=workshop_yaml[Workshop.YamlToken.START],
            end=workshop_yaml[Workshop.YamlToken.END],
            title=workshop_yaml[Workshop.YamlToken.TITLE],
            sessions=[
                Session.from_yaml(entry)
                for entry in workshop_yaml[Workshop.YamlToken.SESSIONS]
            ],
            participants=[
                Participant.from_yaml(entry)
                for entry in workshop_yaml[Workshop.YamlToken.PARTICIPANTS]
            ]
        )

    def store_to_disk(self):

        yaml_data = {
            Workshop.YamlToken.TITLE: self.title,
            Workshop.YamlToken.START: self.start,
            Workshop.YamlToken.END: self.end,
            Workshop.YamlToken.SESSIONS: [
                session.to_yaml()
                for session in self.sessions
            ],
            Workshop.YamlToken.PARTICIPANTS: [
                participant.to_yaml() for participant in self.participants
            ]
        }

        # TODO: store workshop state?

        with open(self.data_file, mode="w") as output_stream:
            yaml.dump(yaml_data, output_stream)
