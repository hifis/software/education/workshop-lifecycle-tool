import logging
from dataclasses import dataclass
from pathlib import Path
from typing import ClassVar, Optional


class Participant:

    @dataclass(frozen=True)
    class JsonToken:
        FULL_NAME: ClassVar[str] = "full_name"
        EMAIL: ClassVar[str] = "email"
        PERSONAL_DATA: ClassVar[str] = "personal_data"
        CHECKED_IN: ClassVar[str] = "checked_in"
        TAGS: ClassVar[str] = "tags"

    @dataclass(frozen=True)
    class YamlToken:
        NUMBER: ClassVar[str] = "number"
        NAME: ClassVar[str] = "name"
        EMAIL: ClassVar[str] = "email"
        CHECKED_IN: ClassVar[str] = "checked_in"
        CERTIFICATE: ClassVar[str] = "certificate"
        CERTIFICATE_SENT: ClassVar[str] = "certificate_sent"
        TAGS: ClassVar[str] = "tags"

    def __init__(
            self, name: str, email: str, tags: set[str],
            checked_in: bool = False,
            certificate: Optional[Path] = None,
            certificate_sent: bool = False
    ):
        """Set up a new participant.

        The email serves as the unique identifier for participants.

        Args:
            name:
                The participants' full name
            email:
                The participants' email to be used as ID and to send messages
            tags:
                Registration tags for the participant.
            checked_in:
                (Default: False) Whether the participant was checked in for the
                workshop.
            certificate:
                (Default: None) The path to the certificate if there is one.
                If the Path seems to be non-existent, it will be reset to None.
            certificate_sent:
                (Default: False) Whether the certificate has been sent via mail.
        """
        if not name:
            raise ValueError("A participants' given name must not be empty")
        if not email:
            raise ValueError("A participants email must not be empty")

        self.name = name
        self.email = email
        self.checked_in = checked_in
        self.tags = tags
        self.certificate_sent = certificate_sent
        self.certificate: Optional[Path] = (
            certificate if certificate and certificate.exists()
            else None
        )

    def merge(self, other: "Participant") -> None:
        """Merge the other participant entry into this one.

        Both entries must have the same email address to be considered related
        to the same participant.
        If one of the two has been checked in, the checked-in flag will be set.
        The `certificate sent` information will be handled in a similar way.
        If the other entry had a valid certificate and this one did not, the
        certificate information will be taken over.

        Args:
            other: Another participant object with which to merge.
        """

        if self.email != other.email:
            logging.warning(
                f"Can not merge participants {self} and {other}: "
                f"Emails do not match"
            )
            return

        self.checked_in = self.checked_in or other.checked_in
        self.certificate_sent = self.certificate_sent or other.certificate_sent
        self.tags = self.tags.union(other.tags)

        if (
                not self.certificate
                and other.certificate and other.certificate.exists()
        ):
            self.certificate = other.certificate

    def has_tag(self, tag: str) -> bool:
        return tag in self.tags

    @property
    def has_certificate(self) -> bool:
        return self.certificate is not None

    @property
    def file_name_slug(self) -> str:
        return f"{self.name.replace(' ', '_')}"

    @property
    def pretty_tags(self) -> str:
        tag_list = list(self.tags)
        tag_list.sort()
        return ", ".join(tag_list)

    @staticmethod
    def from_yaml(raw_yaml: dict) -> "Participant":
        token = Participant.YamlToken

        certificate_text = raw_yaml[token.CERTIFICATE]

        return Participant(
            name=raw_yaml[token.NAME],
            email=raw_yaml[token.EMAIL],
            tags=raw_yaml.get(token.TAGS, set()),
            checked_in=raw_yaml[token.CHECKED_IN],
            certificate=Path(certificate_text) if certificate_text else None,
            certificate_sent=raw_yaml[token.CERTIFICATE_SENT]
        )

    def to_yaml(self) -> dict:
        token = Participant.YamlToken
        return {
            token.NAME: self.name,
            token.EMAIL: self.email,
            token.TAGS: list(self.tags),  # Store it as a list in YAML
            token.CHECKED_IN: self.checked_in,
            token.CERTIFICATE: (
                str(self.certificate) if self.certificate else None
            ),
            token.CERTIFICATE_SENT: self.certificate_sent
        }

    @staticmethod
    def from_indico_json(raw_json: dict) -> "Participant":
        token = Participant.JsonToken

        return Participant(
            name=raw_json[token.FULL_NAME],
            email=raw_json[token.PERSONAL_DATA][token.EMAIL],
            checked_in=raw_json[token.CHECKED_IN],
            tags=set(raw_json[token.TAGS])  # Was stored as a list in YAML
        )
