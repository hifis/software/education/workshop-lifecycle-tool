
from datetime import datetime

from dateutil import parser

from hifis_wolito.model.mixins import LabelledTimespan, TexRepresentable
from hifis_wolito.util import escape_special_characters


class Contribution(LabelledTimespan, TexRepresentable):

    class JsonToken:
        TITLE = "title"
        START = "startDate"
        END = "endDate"
        DESCRIPTION = "description"

    class YamlToken:
        TITLE = "title"
        START = "start"
        END = "end"
        DESCRIPTION = "description"

    def __init__(
            self, title: str,
            start: datetime, end: datetime,
            description: str
    ):
        super().__init__(title, start, end)
        self.description = description

    def as_tex(self, include_description: bool) -> str:
        title = escape_special_characters(self.title)
        content = escape_special_characters(self.description)
        return f"\\item[{title}] {content if include_description else ''}"

    @staticmethod
    def from_indico_json(raw_json: dict) -> "Contribution":
        token = Contribution.JsonToken
        return Contribution(
            title=raw_json.get(token.TITLE, ""),
            start=parser.parse(str(raw_json[token.START]), fuzzy=True),
            end=parser.parse(str(raw_json[token.END]), fuzzy=True),
            description=raw_json.get(token.DESCRIPTION, None)
        )

    def to_yaml(self) -> dict:
        return {
            Contribution.YamlToken.TITLE: self.title,
            Contribution.YamlToken.START: self.start,
            Contribution.YamlToken.END: self.end,
            Contribution.YamlToken.DESCRIPTION: self.description,
        }

    @staticmethod
    def from_yaml(raw_yaml: dict) -> "Contribution":
        return Contribution(
            title=raw_yaml[Contribution.YamlToken.TITLE],
            start=raw_yaml[Contribution.YamlToken.START],
            end=raw_yaml[Contribution.YamlToken.END],
            description=raw_yaml[Contribution.YamlToken.DESCRIPTION]
        )
