import logging
from datetime import timedelta
from typing import Optional

from hifis_wolito.model.mixins import LabelledTimespan, TexRepresentable
from hifis_wolito.model.contribution import Contribution
from hifis_wolito.util import escape_special_characters


class JsonToken:
    """Tokens used in the Indico APIs JSON."""
    CODE = "code"
    CONTRIBUTIONS = "contributions"
    SESSION = "session"
    TITLE = "title"


class YamlToken:
    BLOCKS = "session_blocks"
    CONTRIBUTIONS = "contributions"
    CODE = "code"
    TITLE = "title"


class SessionBlock(LabelledTimespan):
    """ Representation of a session block containing contributions.

    A session block can only take place on the same day.
    While a session itself can span multiple days, a session block is a
    subdivision of that and can not.
    Different session blocks can have an overlapping start and end time.

    A session block where the title is `None` is considered to be a default
    session used to capture contributions that were not assigned to a session.
    """

    def __init__(
            self, title: Optional[str] = None,
            contributions: Optional[list[Contribution]] = None
    ):
        super().__init__(title)

        self.contributions = contributions if contributions else list()
        self.contributions.sort(key=lambda c: c.start)
        if self.contributions:
            self.start = min(self.contributions, key=lambda c: c.start).start
            self.end = max(self.contributions, key=lambda c: c.end).end

    @staticmethod
    def from_yaml(raw_yaml) -> "SessionBlock":
        return SessionBlock(
            title=raw_yaml[YamlToken.TITLE],
            contributions=[
                Contribution.from_yaml(contribution_json)
                for contribution_json in raw_yaml[YamlToken.CONTRIBUTIONS]
            ]
        )

    def to_yaml(self):
        return {
            YamlToken.CONTRIBUTIONS: [
                contribution.to_yaml() for contribution in self.contributions
            ],
            YamlToken.TITLE: self.title
        }


class Session(LabelledTimespan, TexRepresentable):

    def __init__(
            self, title: Optional[str] = None, code: Optional[str] = None,
            session_blocks: Optional[list[SessionBlock]] = None
    ):
        super().__init__(title=title, update_duration=False)
        self.code = code
        self.blocks = session_blocks if session_blocks else []
        self._recalculate_start_end()
        self._recalculate_duration()

    def __repr__(self) -> str:
        return (
            f"Session \"{self.title}\" "
            f"(({self.code if self.code else '---' })), "
            f"{len(self.contributions)} contributions"
        )

    def _recalculate_start_end(self):
        if not self.blocks:
            return
        self.start = min(self.blocks, key=lambda c: c.start).start
        self.end = max(self.blocks, key=lambda c: c.end).end

    def _recalculate_duration(self):
        self._duration = sum(
            [block.duration for block in self.blocks],
            start=timedelta()
        )

    def as_tex(self, contribution_description: bool) -> str:
        title = escape_special_characters(self.title)
        contributions = self.contributions
        contributions.sort(key=(lambda c: c.start))

        if not self.contributions:
            return rf"\section{{{title}}}"

        lines = [
            rf"\section*{{{title}}}",
            r"\begin{description}"
        ] + [
            contribution.as_tex(contribution_description)
            for contribution in contributions
        ] + [
            r"\end{description}"
        ]
        return "\n".join(lines)

    @property
    def contributions(self) -> list[Contribution]:
        return sum([block.contributions for block in self.blocks], start=[])

    @property
    def is_default_session(self) -> bool:
        return self.title is None

    def add_session_blocks(self, blocks: SessionBlock | list[SessionBlock]):
        if not type(blocks) is list:
            blocks = [blocks]
        self.blocks.extend(blocks)
        self._recalculate_start_end()
        self._recalculate_duration()

    @staticmethod
    def compact(to_compact: list["Session"]) -> list["Session"]:
        """Compact a list of sessions.

         This is done by joining those with the same title and code.
         """
        compacted: list[Session] = []
        for unprocessed in to_compact:
            for processed in compacted:
                if (  # Sessions with same title/code can be compacted
                        (unprocessed.title, unprocessed.code) ==
                        (processed.title, processed.code)
                ):
                    processed.add_session_blocks(unprocessed.blocks)
                    break
            else:  # Tried them all, have no session with that title/code yet
                compacted.append(unprocessed)
        return compacted

    @staticmethod
    def from_indico_json(raw_json: dict) -> "Session":

        logging.debug(f"Parse session JSON: {raw_json}")

        block = SessionBlock(
            title=raw_json[JsonToken.TITLE],
            contributions=[
                Contribution.from_indico_json(contrib_json)
                for contrib_json in raw_json[JsonToken.CONTRIBUTIONS]
            ]
        )

        session_data = raw_json[JsonToken.SESSION]
        session = Session(
            title=session_data[JsonToken.TITLE],
            code=session_data.get(JsonToken.CODE, None),
            session_blocks=[block]
        )

        return session

    @staticmethod
    def from_yaml(raw_yaml) -> "Session":
        return Session(
            title=raw_yaml[YamlToken.TITLE],
            code=raw_yaml[YamlToken.CODE],
            session_blocks=[
                SessionBlock.from_yaml(block_yaml)
                for block_yaml in raw_yaml.get(YamlToken.BLOCKS, [])
            ]
        )

    def to_yaml(self):
        return {
            YamlToken.TITLE: self.title,
            YamlToken.CODE: self.code,
            YamlToken.BLOCKS: [
                block.to_yaml() for block in self.blocks
            ]
        }
