import logging
from pathlib import Path

import click

from hifis_wolito.components import workflow_steps
from hifis_wolito.components.cli_printer import CliPrinter
from hifis_wolito.components.email_handling import MailServer
from hifis_wolito.components.indico import IndicoAPI
from hifis_wolito.model.workshop import Workshop
from hifis_wolito.settings import Settings


@click.group()
@click.option(
    "--verbose",
    "-v",
    count=True,
    default=0,
    show_default=True,
    help="Enable verbose output. "
    "Increase verbosity by setting this option up to 4 times.",
)
@click.option(
    "--use-config",
    "-c",
    default=None,
    show_default=False,
    help=f"Specify the path to a config file that is to be used. "
         "By default, the program will look for "
         "{Settings.DEFAULT_CONFIG_FILE.name} in the current working directory."
)
def cli(verbose: int, use_config: Path | None) -> None:

    Settings.set_log_level(verbose)
    Settings.load(use_config)

    if verbose:
        log_level = logging.getLogger().getEffectiveLevel()
        message = f"Verbose logging is enabled. (LEVEL={log_level})"
        click.echo(click.style(message, fg="yellow"))


@cli.command()
@click.argument(
    "workshop_id",
    type=int,
    required=False,
    default=None
)
@click.option(
    "--dump",
    is_flag=True,
    default=False,
    help="Dump the fetched JSON to help with debugging."
)
def fetch(workshop_id: int | None, dump: bool) -> None:
    """Fetch a workshop and all related information from Indico.
    If the workshop is not set up yet, the required files will be created.

    If no specific workshop is given, all workshops on disk will be re-fetched.
    \f

    Args:
         workshop_id: The numeric ID of the workshop in the Indico
         dump: Whether to also generate d dump of the received JSON.
    """
    to_fetch = [workshop_id] if workshop_id else available_workshops()
    if not to_fetch:
        click.echo("Nothing to fetch")
        return

    settings = Settings.get_current_settings()

    indico_endpoint = IndicoAPI(
        endpoint_url=settings.indico.base_url,
        token=settings.indico.token,
        api_key=settings.indico.api_key
    )

    for workshop_id in to_fetch:

        click.echo(f"Fetching workshop {workshop_id} ", nl=False)
        try:
            new_workshop = Workshop.fetch_from_indico(
                indico_api=indico_endpoint,
                workshop_id=workshop_id,
                dump_json=dump
            )

            if not new_workshop.workshop_directory.exists():
                new_workshop.create_structure()

            new_workshop.store_to_disk()
        except Exception as error:
            click.echo(f"→ Error: {error.__class__.__name__}")
        else:
            click.echo("→ Done")
            CliPrinter.print_workshop_details(new_workshop)

        # TODO: if the workshop already is set up,
        #  only update the information from Indico#


@cli.command()
@click.option(
    "--generate",
    "-g",
    is_flag=True,
    show_default=True,
    default=False,
    help="Run the certificate generation if all required information is "
         "available in the workshop folder. Already existing certificates will "
         "be left unchanged"
)
@click.option(
    "--generate-all",
    "-G",
    is_flag=True,
    show_default=True,
    default=False,
    help="Like --generate, but also replaces existing certificates."
)
@click.option(
    "--send-email",
    "-e",
    is_flag=True,
    show_default=True,
    default=False,
    help="Send emails with the certificates to participants, who do not have "
         "received one yet."
)
@click.option(
    "--send-email-all",
    "-E",
    is_flag=True,
    show_default=True,
    default=False,
    help="Like --send-email, but also resends emails to everyone who has a "
         "valid certificate."
)
@click.option(
    "--cc",
    multiple=True,
    default=[],
    help="Email of the recipient of a CC for every certificate sent. "
         "Only takes effect in combination with -e/-E options."
)
@click.option(
    "--dry-run",
    "-d",
    is_flag=True,
    show_default=True,
    default=False,
    help="Only emulates sending the emails and prints their content on the "
         "command line instead of actually sending them. Requires -e or -E"
)
@click.option(
    "--no-descriptions",
    is_flag=True,
    show_default=True,
    default=False,
    help="Disable the output of contribution descriptions in generated "
         "certificates. Only takes effect in combination with -g/-G options."
)
@click.argument("workshop_id", type=int)
def certificates(
        generate: bool, generate_all: bool,
        send_email: bool, send_email_all: bool, cc: list[str],
        dry_run: bool, no_descriptions: bool, workshop_id: int
):
    """Check, generate and email participation certificates for participants.

    If no additional option is given, it will give an overview over the
    existing certificates and the check-in state of participants
    (which should receive the certificates).

    The `generate` or `generate_all` options allow to generate missing
    certificates, with the difference that the latter will override already
    existing certificates with new versions as well.

    Similarly, the `email` or `email_all` options allow to directly send the
    certificates via the email server from the configuration.

    The `dry_run` option allows to test email sending and will print the
    messages to send instead of actually sending them. If neither -e nor -E were
    selected, this option has no effect.
    \f

    Args:
        generate:
            Whether to create missing certificates.
            Superseded by the `generate_all` option.
        generate_all:
            Whether to regenerate all certificates.
            Supersedes the `generate` option.
        send_email:
            Send the certificates via email to those participants which have
            not yet received one.
            Superseded by the `send_email_all` option.
        send_email_all:
            Send the certificates to all participants even if they have received
            some before.
            Supersedes the `send_email` option.
        cc:
            List of CC recipients for sending certificates.
        dry_run:
            If True, only print the emails to send on the console instead
            of actually sending them.
        no_descriptions:
            If true, the output of contribution descriptions will be
            suppressed when generating certificates.
        workshop_id:
            The numeric ID of the workshop as used by Indico.
    """

    # Only checked-in participants should get a certificate
    # Also check if they already have one

    for cc_recipient in cc:
        click.echo(f"Sending CC email to {cc_recipient}")

    # Load model from the workshop folder
    current_workshop = Workshop.fetch_from_disk(workshop_id)

    # === List certification status ===
    if not generate_all or generate or send_email_all or send_email:
        CliPrinter.print_participant_states(
            participants=current_workshop.participants
        )

    # === Generate the missing certificates ===
    if generate or generate_all:
        workflow_steps.certificate_generation(
            current_workshop=current_workshop,
            generate_all=generate_all,
            enable_contribution_descriptions=not no_descriptions
        )

    if not (send_email or send_email_all):  # Nothing left to do
        if dry_run:
            click.echo(
                "Missing option -e or -E, dry run has no effect"
            )
        return

    prepared_emails = workflow_steps.prepare_certificate_emails(
        current_workshop=current_workshop,
        send_to_all=send_email_all,
        cc_addresses=cc
    )

    if not prepared_emails:  # No emails to send
        click.echo("No emails to send")
        return

    # === Send the emails ===
    email_settings = Settings.get_current_settings().email
    if dry_run:
        click.echo(
            click.style("DRY RUN: Sending the following email:", bold=True)
        )
        for recipient, email in prepared_emails.items():
            for field, value in {
                "From": email_settings.sender,
                "To": recipient.email,
                "Reply-To": email_settings.reply_to,
                "CC": ", ".join(cc)
            }.items():
                click.echo(click.style(field + ": ", bold=True), nl=False)
                click.echo(value)
            click.echo(click.style(email, italic=True))
    else:
        mail_server = MailServer(
            host=str(email_settings.hostname),
            port=email_settings.port
        )
        mail_server.login()
        for recipient, email in prepared_emails.items():
            click.echo(f"Sending certificate to {recipient.email}")
            mail_server.send_email(email)
            recipient.certificate_sent = True

    # Save the emailing information
    current_workshop.store_to_disk()


@cli.command()
@click.option(
    "--detail-workshop", "-w",
    type=int,
    default=None,
    help="Show the details for a workshop with a specific ID"
)
def summary(detail_workshop: int | None):
    """
    Generate a summary of all known workshops.
    """

    if detail_workshop is None:
        workshop_ids = available_workshops()

        workshops = []
        for workshop_id in workshop_ids:
            try:
                workshops.append(Workshop.fetch_from_disk(workshop_id))
            except Exception as thrown:
                click.echo(f"Failed to read workshop with id {workshop_id}: "
                           f"{thrown.__class__.__name__}")
                continue

        CliPrinter.print_workshop_overview(workshops)
    else:
        to_show = Workshop.fetch_from_disk(detail_workshop)
        CliPrinter.print_workshop_details(to_show)


def available_workshops():
    """Get the IDs of all workshops currently in the file system.

    No integrity check for the workshop folders is done, use at your own risk.

    Returns:
        A list with the IDs of all found workshops.
    """
    settings = Settings.get_current_settings()

    workshop_ids = []
    for entry in settings.general.workshops_root_folder.iterdir():
        try:
            workshop_ids.append(int(entry.name))
        except ValueError:
            continue

    return workshop_ids

# TODO add command to query workshop state and ToDos
