import logging
import re
from typing import ClassVar, Annotated

from pydantic import BaseModel, HttpUrl, EmailStr, AfterValidator
from pydantic_settings import BaseSettings
from pathlib import Path
import yaml

# Adapted from https://github.com/pydantic/pydantic/issues/7990
DirectoryPath = Annotated[
    Path,
    AfterValidator(lambda v: v.expanduser())
]


class GeneralSettings(BaseModel):
    """Settings for location and folder structure. """
    workshops_root_folder: DirectoryPath = Path.home() / "workshops"
    xelatex_executable: Path
    signature_config_file: str


class IndicoSettings(BaseModel):
    # === Settings for Indico ===
    base_url: HttpUrl
    token: str
    api_key: str


class EmailSettings(BaseModel):
    hostname: str
    port: int = 465
    sender: EmailStr
    reply_to: EmailStr


class TemplateSettings(BaseModel):
    template_folder_name: str
    image_folder_name: str
    """Where the certificate generator looks for images to include."""

    certificate_template: str
    email_text_template: str
    email_footer_template: str
    """An HTML fragment that gets included as the footer in emails sent by the tool.
    It should start and end with the `<footer> … </footer>` tags respectively.
    """

    organizer_logo: str
    """Name of the organizers' logo file.
    It will be looked for in the `CERTIFICATE_IMAGE_PATH`.
    """


class ConfigurationInvalidError(RuntimeError):
    pass


class Settings(BaseSettings):
    """
    Stores the current configuration of the tool.

    To access the settings, use the get_current_settings() for obtaining the
    actual Settings instance.
    """
    general: GeneralSettings
    indico: IndicoSettings
    email: EmailSettings
    templates: TemplateSettings

    _instance = None
    """Stores the actual instance of the currently active settings."""

    VERBOSITY_LEVELS: ClassVar[dict[int, int]] = {
        0: logging.CRITICAL,
        1: logging.ERROR,
        2: logging.WARNING,
        3: logging.INFO,
        4: logging.DEBUG
    }
    MINIMUM_VERBOSITY_LEVEL: ClassVar[int] = 0
    MAXIMUM_VERBOSITY_LEVEL: ClassVar[int] = 4

    # TODO if default config is missing offer to create one by copying

    DEFAULT_CONFIG_FILE: ClassVar[Path] = Path.home() / "workshops" / "wolito-config.yml"

    @classmethod
    def set_log_level(cls, verbosity: int) -> None:
        """
        Configure the logging to use a log level as set in the verbosity setting

        This can be used independently of the currently used configuration.

        Args:
            verbosity:
                The amount of verbosity flags specified, should be
                in the interval 0 ≤ amount ≤ 4. If the interval is exceeded, it
                will be clamped to the closest extreme value.
        """
        minimum = cls.MINIMUM_VERBOSITY_LEVEL
        maximum = cls.MAXIMUM_VERBOSITY_LEVEL
        verbosity = (  # clamp to allowed values
            minimum if verbosity < minimum else
            maximum if verbosity > maximum else
            verbosity
        )
        logging.basicConfig(level=cls.VERBOSITY_LEVELS[verbosity])

    @classmethod
    def get_current_settings(cls) -> "Settings":
        if not cls._instance:
            raise ConfigurationInvalidError("No configuration loaded")
        return cls._instance

    @property
    def workshops_root_folder(self) -> Path:
        """A shortcut to the folder where workshop information is stored."""
        return self.general.workshops_root_folder

    def workshop_folder(self, workshop_id: int) -> Path:
        return self.workshops_root_folder / str(workshop_id)

    @property
    def default_templates_folder(self) -> Path:
        return self.workshops_root_folder / self.templates.template_folder_name

    @staticmethod
    def load(config_file: Path | None = None) -> "Settings":
        """Establish settings from a given config file.

        Args:
            config_file:
                Path to the configuration file to be used. If not specified it
                will look for a file `wolito-config.yml` in the current working
                directory.
        Returns:
            The newly established Settings instance.
        """
        config_file: Path = (
            config_file if config_file
            else Settings.DEFAULT_CONFIG_FILE
        )
        if not config_file.exists():
            raise FileNotFoundError(
                f"Configuration file {config_file.absolute()} not found."
            )

        configuration: dict = {}
        with open(config_file) as input_stream:
            configuration.update(yaml.safe_load(input_stream))
        Settings._instance = Settings(**configuration)
        return Settings(**configuration)
