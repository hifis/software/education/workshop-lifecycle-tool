from datetime import timedelta

from hifis_wolito.constants import (
    HOURS_PER_DAY, SECONDS_PER_HOUR, SECONDS_PER_MINUTE
)

# === TeX specifics ===
TEX_ESCAPE_SEQUENCES = {escapee: "\\" + escapee for escapee in "§$%&{}_"}
"""These signs can be simply escaped in TeX py prefixing a backslash.
See https://en.wikibooks.org/wiki/LaTeX/Special_Characters#Other_symbols
"""


def escape_special_characters(text: str) -> str:
    """Escape special characters in raw text for use in TeX documents.

    Args:
        text: The original raw text to escape.
    Returns:
        A text with all escapable special characters replaced.
    """
    for escape_pair in TEX_ESCAPE_SEQUENCES.items():
        text = text.replace(*escape_pair)
    return text


def pretty_duration(duration: timedelta) -> str:
    """ Generate a visually appealing representation of the duration.

    Returns:
        A pretty-formatted string suitable for printing.
    """

    hours = (
            duration.days * HOURS_PER_DAY +
            duration.seconds // SECONDS_PER_HOUR
    )
    remaining_duration = duration - timedelta(hours=hours)
    minutes = remaining_duration.seconds // SECONDS_PER_MINUTE

    return f"{hours:02}:{minutes:02} hours"
