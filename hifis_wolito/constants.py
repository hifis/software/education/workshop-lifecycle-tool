from datetime import datetime

# This module contains the application-wide constants

# === Miscellaneous ===
SECONDS_PER_MINUTE = 60
SECONDS_PER_HOUR = 3600
HOURS_PER_DAY = 24

# === File type suffixes ===
SUFFIX_PDF = ".pdf"
SUFFIX_TEX = ".tex"


# === Default values for fallbacks ===
DEFAULT_DATETIME: datetime = datetime(
    year=2000, month=1, day=1, hour=0, minute=0
)
