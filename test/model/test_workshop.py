from pathlib import Path

import pytest
import tempfile
import os  # for chdir

from hifis_wolito.model.workshop import Workshop
from hifis_wolito.settings import Settings


@pytest.fixture(autouse=True)
def setup_teardown():
    config_file = Path(__file__).parent / "../fixtures/wolito-config.yml"
    # TODO: The fixed path should be in conftest, shouldn't it?
    assert config_file.exists()

    Settings.load(config_file)
    settings = Settings.get_current_settings()
    with tempfile.TemporaryDirectory() as working_dir:
        cwd = Path(working_dir)
        os.chdir(cwd)
        print(f"Working Directory for testing set to {cwd.absolute()}")

        # Point settings to temporary test folder, create file/folder structure
        settings.general.workshops_root_folder = cwd
        (cwd / settings.templates.template_folder_name).mkdir()
        yield  # Run actual test here


def test_workshop_setup(reference_workshop):
    reference_workshop.create_structure()
    assert reference_workshop.workshop_directory.exists()


def test_yaml_conversion(reference_workshop):
    reference_workshop.create_structure()
    reference_workshop.store_to_disk()
    new_workshop = Workshop.fetch_from_disk(reference_workshop.indico_id)

    assert new_workshop.title == reference_workshop.title
    assert new_workshop.start == reference_workshop.start
    assert new_workshop.end == reference_workshop.end
