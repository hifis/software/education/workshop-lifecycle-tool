from dataclasses import dataclass
from datetime import datetime
from typing import ClassVar

import pytest

from hifis_wolito.model.contribution import Contribution
from hifis_wolito.model.participant import Participant
from hifis_wolito.model.workshop import Workshop


class ReferenceData:
    """
    A constant-only class to store the data used in various fixtures.

    The intention here is to always provide the same values to fixtures
    to keep them compatible when testing representation conversion and the like.

    At the same time it provides a namespace,
     so we don't have all loose constants lying around.
    """

    @dataclass(frozen=True)
    class Participant:
        FIRST_NAMES: ClassVar[str] = "Max Maria"
        LAST_NAMES: ClassVar[str] = "Muster"
        FULL_NAME: ClassVar[str] = f"{FIRST_NAMES} {LAST_NAMES}"
        EMAIL: ClassVar[str] = "muster@example.org"
        CHECKIN_STATUS: ClassVar[bool] = True
        TAGS: ClassVar[set[str]] = {"test"}

    @dataclass(frozen=True)
    class Workshop:
        ID: ClassVar[int] = 0
        TITLE: ClassVar[str] = "Reference Workshop"
        START: ClassVar[datetime] = datetime(2020, 1, 1, 12)
        END: ClassVar[datetime] = datetime(2020, 1, 1, 18)


@pytest.fixture
def reference_contribution() -> Contribution:
    return Contribution(
        title="Test: Reference Contribution",
        start=datetime(year=2000, month=1, day=1, hour=0),
        end=datetime(year=2000, month=1, day=1, hour=1),
        description="Description of reference contribution"
    )


@pytest.fixture
def reference_participant() -> Participant:
    reference = ReferenceData.Participant
    return Participant(
        name=reference.FULL_NAME,
        email=reference.EMAIL,
        checked_in=reference.CHECKIN_STATUS,
        tags=reference.TAGS
    )


@pytest.fixture
def reference_workshop(
        reference_participant
) -> Workshop:
    reference = ReferenceData.Workshop
    yield Workshop(
        indico_id=reference.ID,
        title=reference.TITLE,
        participants=[reference_participant],
        start=reference.START,
        end=reference.END
    )


@pytest.fixture
def reference_participant_json() -> dict:
    reference = ReferenceData.Participant
    return {
        'checked_in': reference.CHECKIN_STATUS,
        'checkin_secret': '123456789-abcd-abcd-abcd-0123456789a',
        'full_name': reference.FULL_NAME,
        'personal_data': {
            'country': '',
            'country_code': '',
            'email': reference.EMAIL,
            'firstName': reference.FIRST_NAMES,
            'phone': '',
            'surname': reference.LAST_NAMES
        },
        'registrant_id': '31415',
        'tags': list(reference.TAGS)
    }
