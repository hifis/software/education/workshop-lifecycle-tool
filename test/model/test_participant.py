
import tempfile
import yaml

from hifis_wolito.model.participant import Participant


def test_file_name_slug(reference_participant):
    expected = reference_participant.name.replace(" ", "_")
    assert reference_participant.file_name_slug == expected


def test_yaml_conversion(reference_participant):

    as_yaml = reference_participant.to_yaml()
    with tempfile.TemporaryFile(mode="w+") as iostream:
        yaml.dump(as_yaml, iostream)
        iostream.seek(0)  # Reset to the front
        loaded_yaml = yaml.load(iostream, yaml.SafeLoader)
    uut = Participant.from_yaml(loaded_yaml)

    assert uut.name == reference_participant.name
    assert uut.email == reference_participant.email
    assert uut.checked_in == reference_participant.checked_in
    assert uut.certificate == reference_participant.certificate
    assert uut.certificate_sent == reference_participant.certificate_sent


def test_indico_json_conversion(
        reference_participant_json, reference_participant
):
    uut = Participant.from_indico_json(reference_participant_json)

    assert uut.name == reference_participant.name
    assert uut.email == reference_participant.email
    assert uut.checked_in == reference_participant.checked_in
    assert uut.certificate == reference_participant.certificate
    assert uut.certificate_sent == reference_participant.certificate_sent
