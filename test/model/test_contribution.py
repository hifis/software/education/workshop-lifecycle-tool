
import tempfile

import yaml

from hifis_wolito.model.contribution import Contribution


def test_yaml_conversion(reference_contribution):

    as_yaml = reference_contribution.to_yaml()
    with tempfile.TemporaryFile(mode="w+") as iostream:
        yaml.dump(as_yaml, iostream)
        iostream.seek(0)  # Reset to the front
        loaded_yaml = yaml.load(iostream, yaml.SafeLoader)
    new_contribution = Contribution.from_yaml(loaded_yaml)

    assert new_contribution.title == reference_contribution.title
    assert new_contribution.start == reference_contribution.start
    assert new_contribution.end == reference_contribution.end

