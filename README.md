# Workshop Lifecycle Tool

This is the workshop lifecycle tool created by HIFIS.
It builds upon a workflow centered around the 
[Indico][indico_home] event management tool. 

## General Information

### License

This tool is published under the MIT License. 
See the [`LICENSE` file](./LICENSE) for more information.

### Intended Audience

HIFIS instructors or anyone who organizes workshops with Indico 
and could use some automation to support one or multiple encompassing steps.

## How it works

The _Workshop Lifecycle Tool_ stores its workshop information in a dedicated
directory (`~/workshops` by default).
Inside, each workshop has its own directory, named after the numeric ID 
that the workshop has in the Indico system. 
These directories store all information related to the respective workshops.

Further, a `templates` subdirectory stores the various presets the tool uses,
like Email-templates, XeLaTeX-templates for the certificates etc.

On creation of a workshop these templates will be copied 
into the workshop directory, so they can be adapted on a per-workshop basis.

## How to set up and Configure the Tool

Inside the workshop directory the configuration for the tool resides in the file
`wolito-config.yml`.
This files controls the settings for the application.

To set up the application:

1. Create the `~/workshops` directory if it does not yet exist.
2. Copy the `resources/wolito-config.yml` from this project to create 
the starting configuration `~/workshops/wolito-config.yml`.
3. Adapt `~/workshops/wolito-config.yml` to your needs and setup.

**Note** that you will need an Indico Oauth-Token and API-Key that identifies 
you with the Indico system you are using.
These can be generated in your Indico user settings and allow the tool to access
the events in the same function as you can access them as the user.

## How to Use

The main command is `hifis-wolito`. 
It is to be followed by one of the subcommands specified below 
to execute a step in the workshop management workflow.

The tool is split into several subcommands 
which will be explained in detail below.

Syntax:
: `hifis-wolito [-v] <subcommand>`

Currently available subcommands are

* `fetch`
* `certificates`

#### The Indico ID

Most subcommands operate using the `<indico id>` of a workshop.
This ID is a number given by indico to identify an event.
It can be obtained by looking at the URL of an event.

> **Example:**
>
> If the URL of an event is `https://indico.example.org/event/123/`,
> then the Indico ID of that event would be _123_. 

All subcommands support the `--help` option to give additional information
on their arguments and how to use them.

#### Debug Settings

The `-v` option can be repeated up to 4 times to increase the verbosity setting.
A higher verbosity will prompt the tool to give a more detailed output
on its current operations.

### The `fetch` Subcommand

Syntax:
: `hifis-wolito fetch <indico id>`

This command will look up the Indico server (configured in `settings.py`) for
the event with the given ID.
If the event is found, the participant and contribution data for that event
will be stored in the workshop directory.

**Note:** 
Currently all previously existing workshop information will be replaced.

### The `certificates` Subcommand

Syntax:
: `hifis-wolito certificates [-deEgG] <indico id>`

This command manages the participation certificates.
If none of the additional options is given, only a list of participants, 
their check-in state, generated certificates and whether these have been sent 
already will be given.

Options in lower-case will only appliy to participants whom have not been 
processed already, while upper-case options will be applied 
to _all_ participants.

#### Certificate Generation

The `-g` and `-G` options will trigger the generation of the certificates.

> **Note:** Only participants that are marked as _checked in_ within Indico
> will receive certificates in any case.

For each participant in the selected scope 
(`-g`: only those without certificates, `-G`: all participants)
the certificate template from the workshop will be filled in with the 
participant data and converted to a PDF file with _XeLaTeX_.
The then generated certificates will then be signed according to the 
configuration in the `signature.yml` file.

**Note:** If the key file used for the signature requires a password,
it will be prompted during the execution of this step.

The generated and signed certificates will be stored 
in the `certificates` subdirectory.

##### Certification Rules

The tool will automatically check for which contributions a participant 
can be certified.

1. A participant that is not checked-in in Indico can **never** be certified.
2. Contributions that are not within a session are considered to use the
  _default session_ and will not be included in the certificate.
3. If a participant has _no_ tags, they get certified for all sessions
   except the _default session_.
4. If a participant has tags, all sessions are certified whose session code 
   is in the participants' tags.

#### Sending Certificates via Email

Using the `-e` and `-E` options, you can send the certificates via email to 
the participants. With the `-e` only those who have not been marked as messaged
yet will be sent an Email, while the `-E` option will send certificates to 
all participants for which one has been generated.

Adding the `-d` option prompts a dry run which will only output previews 
of the mails that would be sent.

**Note:** When sending the emails, the login data for the mail server 
will be prompted during the execution of this command.

The configuration for the email sender can be found in `settings.py` and the
template for the message itself is contained as an HTML-file in the workshops'
template directory.


[indico_home]: https://getindico.io/
